# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) if char == char.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.length.odd? ? str[str.length/2] : [str[str.length/2 - 1], str[str.length/2]].join
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    count += 1 if VOWELS.include?(char)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(&:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return "" if arr.empty?
  joined = ""
  arr[0...-1].each{|x| joined << x + separator}
  joined + arr.last
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.split("").each_with_index do |el, idx|
    idx % 2 == 1 ? str[idx] = el.upcase : str[idx] = el.downcase
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split
  str.map{|word| word.length >= 5 ? word.reverse : word}.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |num|
    if num % 15 == 0
      num = "fizzbuzz"
    elsif num % 5 == 0
      num = "buzz"
    elsif num % 3 == 0
      num = "fizz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  array = []
  i = arr.length - 1
  until i < 0
    array << arr[i]
    i -= 1
  end
  array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  factors = []
  (1..num).to_a.each do |number|
    factors << number if num % number == 0
  end
  factors.size == 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).to_a.each do |number|
    factors << number if num % number == 0
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  primes = []
  factors(num).each do |number|
    primes << number if prime?(number)
  end
  primes
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  arr.count{|num| num % 2 == 0} > 1 ? arr.select{|num| num.odd?}[0] : arr.select{|num| num.even?}[0]
end
